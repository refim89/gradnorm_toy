import torch
from torch import nn

from data_generator import DataGenerator
from fc_model import FcModel

if __name__ == '__main__':

    model = FcModel()
    model = model.cuda()

    data_generator = DataGenerator(batch_size=64)

    loss_model = nn.MSELoss()
    loss_balancer = nn.L1Loss()

    weights = [torch.tensor([1.0], requires_grad=True), torch.tensor([1.0], requires_grad=True)]
    weights = [w.cuda().detach().requires_grad_(True) for w in weights]

    optimizer_model = torch.optim.Adam(model.parameters(), lr=0.001)
    optimizer_balancer = torch.optim.SGD(weights, lr=0.001)

    alpha = 0.12
    model.train()

    for step, batch in enumerate(data_generator):
        x, y = batch
        x = torch.tensor(x, dtype=torch.float).cuda()
        y = [torch.tensor(target, dtype=torch.float).cuda() for target in y]

        # x = torch.tensor(x, dtype=torch.float)
        # y = [torch.tensor(target, dtype=torch.float) for target in y]

        predictions = model(x)

        losses = [wi.item() * loss_model(pi, yi) for wi, pi, yi in zip(weights, predictions, y)]

        if (step + 1) % 1000 == 0:
            task_losses = [l.item() for l in losses]
            print(step + 1, task_losses, weights)

        loss = sum(losses)
        optimizer_model.zero_grad()
        optimizer_balancer.zero_grad()

        if step == 0:
            l0_losses = [l.item() for l in losses]
            # print(l0_losses)
        else:
            l_hat = [l.item() / l0 for l, l0 in zip(losses, l0_losses)]
            l_hat_avg = (l_hat[0] + l_hat[1]) / 2

            inv_rate = [l_hat_task / l_hat_avg for l_hat_task in l_hat]

            g1 = torch.autograd.grad(losses[0], model.shared_layer.parameters(), create_graph=True)
            g2 = torch.autograd.grad(losses[1], model.shared_layer.parameters(), create_graph=True)

            g1_norm = weights[0] * torch.tensor(torch.norm(g1[0]).item(), requires_grad=False)
            g2_norm = weights[1] * torch.tensor(torch.norm(g2[0]).item(), requires_grad=False)
            g_avg = ((g1_norm + g2_norm) / 2)

            g_target = [g_avg * pow(r, alpha) for r in inv_rate]
            loss_gradnorm = loss_balancer(g1_norm, g_target[0]) + loss_balancer(g2_norm, g_target[1])

            loss_gradnorm.backward()
            optimizer_balancer.step()

            # print(g_target, "\t", loss_gradnorm.data, "\t", weights)

            w_abs = [torch.abs(w) for w in weights]

            weights_sum = sum(w_abs).item()
            weights[0].data = 2 * w_abs[0].data / weights_sum
            weights[1].data = 2 * w_abs[1].data / weights_sum

        loss.backward()
        optimizer_model.step()

        if step > 100000:
            break
