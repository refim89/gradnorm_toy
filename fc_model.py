import torch
from torch import nn


class FcModel(nn.Module):
    def __init__(self, num_tasks=2):
        super(FcModel, self).__init__()

        self.num_tasks = num_tasks

        self.features_layer = nn.Sequential(
            nn.Linear(250, 105, bias=False),
            nn.ReLU(),
            nn.Linear(105, 104, bias=False),
            nn.ReLU(),
        )

        self.shared_layer = nn.Sequential(nn.Linear(104, 103, bias=False),
                                          nn.ReLU())

        self.task_predictions_1 = nn.Linear(103, 100, bias=False)
        self.task_predictions_2 = nn.Linear(103, 100, bias=False)

        self.task_predictions = [self.task_predictions_1, self.task_predictions_2]

    def forward(self, x):
        features = self.features_layer(x)
        shared = self.shared_layer(features)
        predictions = [linear(shared) for linear in self.task_predictions]
        return predictions


if __name__ == '__main__':
    import numpy as np

    model = FcModel(2)

    x = torch.tensor(np.random.rand(32, 250), dtype=torch.float)

    predictions = model(x)

    print([p.shape for p in predictions])
