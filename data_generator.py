import numpy as np


class DataGenerator:
    def __init__(self, sigmas=(1.0, 100.0), batch_size=32, seed=2896):
        np.random.seed(seed)
        self.B = 10.0 * np.random.randn(250, 100)
        self.num_tasks = len(sigmas)
        self.eps = [3.5 * np.random.rand(250, 100) for _ in range(self.num_tasks)]
        self.sigmas = sigmas
        self.batch_size = batch_size

    def __next__(self):
        x = np.random.rand(self.batch_size, 250)
        outputs = []
        for i in range(self.num_tasks):
            f = self.sigmas[i] * np.tanh(np.dot(x, self.B + self.eps[i]))
            outputs.append(f)

        return x, outputs

    def __iter__(self):
        return self


if __name__ == '__main__':
    data_generator = DataGenerator()

    for batch in data_generator:
        x, outputs = batch

        print(x.shape, [y.shape for y in outputs])
        break
