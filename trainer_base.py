import torch
from torch import nn

from data_generator import DataGenerator
from fc_model import FcModel

if __name__ == '__main__':

    model = FcModel()
    model = model.cuda()

    data_generator = DataGenerator(batch_size=64)

    loss_func = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    for step, batch in enumerate(data_generator):
        model.train()

        x, y = batch
        x = torch.tensor(x, dtype=torch.float).cuda()
        y = [torch.tensor(target, dtype=torch.float).cuda() for target in y]

        predictions = model(x)

        losses = [loss_func(pi, yi) for pi, yi in zip(predictions, y)]

        if (step + 1) % 1000 == 0:
            task_losses = [l.item() for l in losses]
            print(step + 1, task_losses)

        loss = sum(losses)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if step > 100000:
            break
