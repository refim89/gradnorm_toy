import torch
from torch import nn


def one_output():
    loss_model = nn.MSELoss()

    weights = torch.tensor([1.0], requires_grad=True).cuda().detach().requires_grad_(True)
    optimizer = torch.optim.SGD([weights], lr=0.001)

    target = torch.tensor([2.3]).cuda()

    for _ in range(10):
        optimizer.zero_grad()
        loss = loss_model(weights, target)
        loss.backward()
        print(loss.item(), weights.grad, weights)

        optimizer.step()

        print(loss.item(), weights.grad, weights)


def two_outputs():
    loss_model = nn.MSELoss()

    b0 = torch.tensor(1.0, requires_grad=True)
    b10 = torch.tensor(1.0, requires_grad=True)
    b11 = torch.tensor(1.0, requires_grad=True)

    optimizer = torch.optim.SGD([b0, b10, b11], lr=0.001)

    targets = torch.tensor([2.3, -1.7])

    for _ in range(10):
        optimizer.zero_grad()

        losses = [loss_model(b0 + b10, targets[0]), loss_model(b0 + b11, targets[1])]
        loss = sum(losses)

        g1 = torch.autograd.grad(losses[0], b0, retain_graph=True, create_graph=True)

        loss.backward()
        optimizer.step()

        print(loss.item(), b0.grad, b10.grad, b11.grad, b10.grad + b11.grad)
        # print(loss.item(), b0.grad, b10.grad, b11.grad, b0, b10, b11)

if __name__ == '__main__':
    one_output()
    # two_outputs()
